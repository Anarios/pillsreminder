module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-coffee');

    grunt.initConfig({
        coffee: {
            compile: {files: {'app/scripts/dist/compiled-coffee.js': ['app/scripts/src/**/*.coffee']}}
        }
        , concat: {
            dist: {
                src: [
                    'app/bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js'
                    , 'app/bower_components/angular/angular.js'
                    , 'app/bower_components/angular-animate/angular-animate.js'
                    , 'app/bower_components/angular-messages/angular-messages.js'
                    , 'app/bower_components/angular-ui-router/release/angular-ui-router.js'
                    , 'app/bower_components/angular-aria/angular-aria.js'
                    , 'app/bower_components/angular-material/angular-material.js'
                    , 'app/bower_components/angular-toastr/dist/angular-toastr.tpls.js'
                    , 'app/bower_components/angular-toastr/dist/angular-toastr.js'
                    , 'app/scripts/dist/compiled-coffee.js'
                ],
                dest: 'app/scripts/dist/script.js'
            }
        }
        , sass: {
            options: {
                check: true,
                sourceMap: true
            },
            dist: {
                files: {
                    'app/styles/dist/style.css': 'app/styles/src/styles.scss'
                }
            }
        }
        
        , watch: {
            scripts: {
                files: ['app/scripts/src/**/*.coffee'],
                tasks: ['coffee', 'concat'],
                options: {spawn: false}
            }
            , styles: {
                files: ['app/styles/src/style.scss'],
                tasks: ['sass', 'autoprefixer']
            }
        }

    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['coffee', 'concat', 'sass']);
    grunt.registerTask('watch', ['coffee', 'concat', 'sass']);

    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-watch');
};