﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PillsReminder.Startup))]
namespace PillsReminder
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
