using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PillsReminder.Models;

namespace PillsReminder.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using PillsReminder.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<PillsReminder.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "PillsReminder.Models.ApplicationDbContext";
        }

        protected override void Seed(PillsReminder.Models.ApplicationDbContext context)
        {
            const string doctorRoleName = "Doctor";
            const string patientRoleName = "Patient";

            var roleStore = new RoleStore<AspnetRole, string, AspnetUserRole>(context);
            var roleManager = new RoleManager<AspnetRole, string>(roleStore);

            if (!roleManager.RoleExists(doctorRoleName))
            {
                roleManager.Create(new AspnetRole
                {
                    Id = "fa012009-406c-47c1-9115-ca7088324915",
                    Name = doctorRoleName
                });
            }
            if (!roleManager.RoleExists(patientRoleName))
            {
                roleManager.Create(new AspnetRole
                {
                    Id = "6fdbcce1-fbdc-4b15-b4c0-aa69cd2a0f7e",
                    Name = patientRoleName
                });
            }

            var userStore = new UserStore<User, AspnetRole, string, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>(context);
            var userManager = new UserManager<User, string>(userStore);

            if (!context.Users.Any(x => x.Email == "doctor@rhok.com"))
            {
                var doctor = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Doctor",
                    LastName = "Who",
                    Email = "doctor@rhok.com",
                    UserName = "doctor@rhok.com",
                    Schedule = new Schedule
                    {
                        Id = 1000,
                        Name = "Schedule1"
                    },
                    DateBorn = new DateTime(2000, 1, 1)
                };

                userManager.Create(doctor, "Qq!456");
                userManager.AddToRole(doctor.Id, doctorRoleName);
            }

            if (!context.Users.Any(x => x.Email == "testdoctor@mail.ru"))
            {
                var doctor = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    FirstName = "Doctor",
                    LastName = "Test",
                    Email = "testdoctor@mail.ru",
                    UserName = "testdoctor@mail.ru",
                    Schedule = new Schedule
                    {
                        Id = 1000,
                        Name = "Schedule1"
                    },
                    DateBorn = new DateTime(2000, 1, 1)
                };

                userManager.Create(doctor, "123456");
                userManager.AddToRole(doctor.Id, doctorRoleName);
            }

            if (!context.Users.Any(x => x.UserName == "testpatient@mail.ru"))
            {
                var patient = new User
                {
                    Id = "testpatient",
                    FirstName = "Test",
                    LastName = "Patient",
                    Email = "testpatient@mail.ru",
                    UserName = "testpatient@mail.ru",
                    Schedule = new Schedule
                    {
                        Id = 1000,
                        Name = "Schedule1"
                    },
                    DateBorn = new DateTime(2000, 1, 1),
                    PillReceivings = new List<PillReceiving>() {
                        new PillReceiving()
                        {
                            DateTaken = new DateTime(2016, 03, 26) 
                        },
                        new PillReceiving()
                        {
                            DateTaken = new DateTime(2016, 03, 26) 
                        },
                        new PillReceiving()
                        {
                            DateTaken = new DateTime(2016, 03, 27) 
                        },
                        new PillReceiving()
                        {
                            DateTaken = new DateTime(2016, 03, 27) 
                        },
                    }
                };

                userManager.Create(patient, "Qq!456");
                userManager.AddToRole(patient.Id, patientRoleName);
            }
        }
    }
}
