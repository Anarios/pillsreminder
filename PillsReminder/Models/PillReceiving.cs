﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PillsReminder.Models
{
    public class PillReceiving
    {
        public long Id { get; set; }
        public virtual User Patient { get; set; }
        public DateTime DateTaken { get; set; }

    }
}