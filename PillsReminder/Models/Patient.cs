using System;

namespace PillsReminder.Models
{
    //public class Patient
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string LastName { get; set; }
    //    public string Password { get; set; }
    //    public string Login { get; set; }
    //    public DateTime DateBorn { get; set; }
    //    public string PhoneNumber { get; set; }
    //    public Schedule Schedule { get; set; }
    //    public PatientStatus Status { get; set; }
    //}

    public enum PatientStatus
    {
        AllPillsTaken = 0,
        AcceptableMissed = 1,
        ManyMissed = 2
    }
}