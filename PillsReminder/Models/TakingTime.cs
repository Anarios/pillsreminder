﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PillsReminder.Models
{
    public class TakingTime
    {
        public int Id { get; set; }
        [DataType(DataType.Time)]
        public DateTime Time { get; set; }
        public string Description { get; set; }
    }
}