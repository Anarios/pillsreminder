﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PillsReminder.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser<string, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, string> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateBorn { get; set; }
        public Schedule Schedule { get; set; }
        public PatientStatus Status { get; set; }
        public virtual ICollection<PillReceiving> PillReceivings { get; set; } 
    }
    public class AspnetUserClaim : IdentityUserClaim<string>
    {
    }

    public class AspnetUserLogin : IdentityUserLogin<string>
    {
    }
    public class AspnetUserRole : IdentityUserRole<string>
    {
    }

    public class AspnetRole : IdentityRole<string, AspnetUserRole>
    {
    }

    public class ApplicationDbContext : IdentityDbContext<User, AspnetRole, string, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<PillReceiving> PillReceivings { get; set; }
        public DbSet<TakingTime> TakingTimes { get; set; }
    }
}