﻿using System;
using System.Collections.Generic;

namespace PillsReminder.Models
{
    public class Schedule
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<TakingTime> TakingTimes { get; set; }
    }
}