﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using PillsReminder.Models;
using PillsReminder.ViewModels;
using WebGrease.Css.Extensions;

namespace PillsReminder.Controllers
{
    public class PatientsController : BaseController
    {
        private UserStore<User, AspnetRole, string, AspnetUserLogin, AspnetUserRole, AspnetUserClaim> userStore;
        private UserManager<User, string> userManager;

        public PatientsController()
        {
            userStore = new UserStore<User, AspnetRole, string, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>(db);
            userManager = new UserManager<User, string>(userStore);
        }
        [Authorize(Roles = "Doctor")]
        // GET: Patients
        public async Task<ActionResult> Index()
        {
            var db_patients = await db.Users.Where(u => u.FirstName != "Doctor").ToListAsync();
            var patients = db_patients.Select(p => new PatientViewModel(p));

            return View(patients);
        }

       
        public async Task<ActionResult> GetPatientsList()
        {
            var db_patients = await db.Users.ToListAsync();
            var patients = db_patients.Select(p => new PatientViewModel(p));

            return Json(patients, JsonRequestBehavior.AllowGet);
        }

        // GET: Patients/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id.IsNullOrWhiteSpace())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User patient = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(new PatientViewModel(patient));
        }

        public async Task<ActionResult> GetPatientDetails(string id)
        {
            if (id.IsNullOrWhiteSpace())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User patient = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return Json(new PatientViewModel(patient), JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Authorize(string login, string password)
        {
            bool result = await db.Users.AnyAsync(p => p.UserName == login);
            return Json(new { authorized = true }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetPatientSchedule(string login, string password)
        {
            List<DateTime> times = new List<DateTime>()
            {
                new DateTime(1, 1, 1, 10, 0, 0),
                new DateTime(1, 1, 1, 12, 0, 0),
                new DateTime(1, 1, 1, 16, 30, 0),
                new DateTime(1, 1, 1, 20, 00, 0),
            };

            var items = times.Select(x => new { Time = x.ToString("HH:mm"), Description = "Показать такое сообщение" });
            return Json(items, JsonRequestBehavior.AllowGet);
        }

        // GET: Patients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Patients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(PatientViewModel patient)
        {
            var db_patient = patient.ToDbObject();
           
            db_patient.Id = Guid.NewGuid().ToString();
            db_patient.Email = db_patient.UserName;
            var result = await userManager.CreateAsync(db_patient, patient.Password);
            await userManager.AddToRoleAsync(db_patient.Id, "Patient");
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }

            return View(patient);
        }

        // GET: Patients/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id.IsNullOrWhiteSpace())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User patient = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(new PatientViewModel(patient));
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(PatientViewModel patient)
        {
           
                var db_patient = patient.ToDbObject();
                db_patient.PasswordHash = "asdasd";
                db.Entry(db_patient).State = EntityState.Modified;
                await db.SaveChangesAsync();
                await userManager.RemovePasswordAsync(db_patient.Id);
                await userManager.AddPasswordAsync(db_patient.Id, "123456");

                return RedirectToAction("Index");
            
            return View(patient);
        }

        // GET: Patients/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id.IsNullOrWhiteSpace())
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User patient = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(new PatientViewModel(patient));
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            User patient = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            db.Users.Remove(patient);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public async Task<ActionResult> TakePill(string login, string password, string dateTaken)
        {
            var userEntity = await userManager.FindByNameAsync(login);
            var user = await db.Users.FirstAsync(x => x.Id == userEntity.Id);
            var result = await userManager.CheckPasswordAsync(user, password);
            if (result)
            {
                DateTime dateTakenParced = DateTime.Parse(dateTaken);
                user.PillReceivings.Add(new PillReceiving() { DateTaken = dateTakenParced });
                await db.SaveChangesAsync();
            }
            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Patient")]
        public async Task<ActionResult> TakePills()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Patient")]
        public async Task<ActionResult> TakeFlower()
        {
            var userId = User.Identity.GetUserId();
            var user = db.Users.First(x => x.Id == userId);
          
            user.PillReceivings.Add(new PillReceiving() { DateTaken = DateTime.Now });
            await db.SaveChangesAsync();
            
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> ViewChart(string userId)
        {
            var user = db.Users.First(x => x.Id == userId);
            var takenDays = user.PillReceivings.Select(x => x.DateTaken);
            var dict = new Dictionary<string, List<string>>();
            foreach (var takenDay in takenDays)
            {
                if (!dict.ContainsKey(takenDay.ToString("yyyy-MM-dd")))
                {
                    dict[takenDay.ToString("yyyy-MM-dd")] = new List<string>
                    {
                        takenDay.ToString("hh:mm")
                    };
                }
                else
                {
                    dict[takenDay.ToString("yyyy-MM-dd")].Add(takenDay.ToString("hh:mm"));
                }
            }

            ViewBag.ChartData = JsonConvert.SerializeObject( new TakenPills
            {
                User = user.FirstName + " " + user.LastName,
                TakenDays = dict.Select(day => new Day
                {
                    Date = day.Key,
                    Times = day.Value
                }).ToList()
            });
            return View();
        }

        public async Task<ActionResult> TakenPillsList()
        {
            var list = await db.PillReceivings.ToListAsync();
            return View(list);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
