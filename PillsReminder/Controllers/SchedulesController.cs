﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PillsReminder.Models;
using PillsReminder.ViewModels;

namespace PillsReminder.Controllers
{
    public class SchedulesController : BaseController
    {
        // GET: Schedules
        public async Task<ActionResult> Index()
        {
            var db_schedules = await db.Schedules.ToListAsync();
            var schedules = db_schedules.Select(s => new ScheduleViewModel(s));

            return View(schedules);
        }

        // GET: Schedules/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = await db.Schedules.Include(t => t.TakingTimes).FirstOrDefaultAsync(s => s.Id == id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(new ScheduleViewModel(schedule));
        }

        // GET: Schedules/Create
        public ActionResult Create()
        {
            return View(new ScheduleViewModel());
        }

        // POST: Schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ScheduleViewModel schedule)
        {
            if (ModelState.IsValid)
            {
                var db_schedule = schedule.ToDbObject();
                db.Schedules.Add(db_schedule);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(schedule);
        }

        // GET: Schedules/Edit/
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = await db.Schedules.Include(t => t.TakingTimes).FirstOrDefaultAsync(s => s.Id == id);
            if(schedule.TakingTimes.Count <= 0)
                schedule.TakingTimes = new List<TakingTime>{new TakingTime()};

            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(new ScheduleViewModel(schedule));
        }

        // POST: Schedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ScheduleViewModel schedule)
        {
            if (ModelState.IsValid)
            {
                var dbSchedule = db.Schedules.First(x => x.Id == schedule.Id);

                db.TakingTimes.RemoveRange(dbSchedule.TakingTimes);
                dbSchedule.TakingTimes = schedule.TakingTimes;
                dbSchedule.Name = schedule.Name;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(schedule);
        }

        // GET: Schedules/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = await db.Schedules.Include(t => t.TakingTimes).FirstOrDefaultAsync(s => s.Id == id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(new ScheduleViewModel(schedule));
        }

        // POST: Schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Schedule schedule = await db.Schedules.Include(t => t.TakingTimes).FirstOrDefaultAsync(s => s.Id == id);
            db.Schedules.Remove(schedule);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public ActionResult AddRow()
        {
            return PartialView("~/Views/Shared/EditorTemplates/TakingTimesRow.cshtml", new ScheduleViewModel());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
