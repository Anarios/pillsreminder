﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;
using PillsReminder.Models;

namespace PillsReminder.ViewModels
{
    public class PatientViewModel
    {
        public PatientViewModel()
        {
        }

        public PatientViewModel(User patient)
        {
            Id = patient.Id;
            Name = patient.FirstName;
            LastName = patient.LastName;
            Login = patient.UserName;
            Password = patient.PasswordHash;
            DateBorn = patient.DateBorn;
            PhoneNumber = patient.PhoneNumber;
            // Schedule = patient.Schedule;
            Status = patient.Status;
        }

        public User ToDbObject()
        {
            return new User
            {
                Id = this.Id,
                FirstName = this.Name,
                LastName = this.LastName,
                UserName = this.Login,
                DateBorn = this.DateBorn,
                PhoneNumber = this.PhoneNumber,
                // Schedule = this.Schedule
                Status = Status
            };
        }

        [Required]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Расписание")]
        public string Schedule { get; set; }

        [Display(Name = "Дата Рождения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateBorn { get; set; }

        [Display(Name = "Телефон")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Статус")]
        public PatientStatus Status { get; set; }
    }
}