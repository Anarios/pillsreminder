﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PillsReminder.Models;

namespace PillsReminder.ViewModels
{
    public class ScheduleViewModel
    {
        public ScheduleViewModel()
        {
            TakingTimes = new List<TakingTime>{new TakingTime{Time = new DateTime()}};
        }

        public ScheduleViewModel(Schedule schedule)
        {
            Id = schedule.Id;
            Name = schedule.Name;
            TakingTimes = schedule.TakingTimes;
        }

        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Время приёма лекарства")]
        public virtual List<TakingTime> TakingTimes { get; set; }

        public Schedule ToDbObject()
        {
            return new Schedule
            {
                Id = this.Id,
                Name = this.Name,
                TakingTimes = this.TakingTimes
            };
        }
    }
}