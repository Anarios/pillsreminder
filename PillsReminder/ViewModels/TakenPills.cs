﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PillsReminder.ViewModels
{
    public class TakenPills
    {
        public string User { get; set; }

        public IEnumerable<Day> TakenDays { get; set; }

    }

    public class Day
    {
        public string Date { get; set; }
        public IList<string> Times { get; set; }
    }
}